import React from 'react'
import { Dropdown, Grid, Button, Loader } from 'semantic-ui-react'

export default function CuisineSelection({options, handleChange, handleSuggestion}) {
    if (!!options && options.length === 0){
        return(<Loader active inline='centered' content='Loading' size='large'/>) 
    }else{
        return( 
            <div>
                <h3 id="cuisine-header">Select cuisines you might be interested in or let us pick a restaurant for you</h3>
                <Grid centered columns={2}>
                    <Grid.Row>
                        <Grid.Column  width={3}>
                            <Dropdown id="cuisine-dropdown"   
                            placeholder='Cuisine'
                            fluid
                            multiple
                            search
                            selection
                            onChange = {handleChange}
                            options={options}/>
                        </Grid.Column>
                    
                        <Grid.Column width={3} >  
                            <Button color="green" id="suggestion-button" onClick={handleSuggestion}>Hit me</Button>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </div>
        )

    }
    
}
