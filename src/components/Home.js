import React, {useState} from 'react'
import {currentLocation, zipToLatLong} from "../services/zipCodes"
import {getCuisines, getRestaurants} from "../services/zomatoCalls"
import {Button, Icon, Image} from 'semantic-ui-react'
import CuisineSelection from './CuisineSelection'
import DisplayRestaurant from './DisplayRestaurant'


export default function Home() {
    const [zipInput, toggleZip] = useState(false)
    const [zip, setZip] = useState("")
    const [options, setOptions] = useState([])
    const [cuisines, setCuisines] = useState([])
    const [latLng, setLatLng] = useState({})
    const [restautants, setRestaurants] = useState([])
    const [restToDisplay, setRestToDisplay] = useState({})
    const [counter, setCounter] = useState(0)

    const handleChange = (e,{value}) =>{
        setCuisines(value)
    }
    

    const handleSuggestion = () =>{
        setRestToDisplay({name:"loading"})
        if (restautants.length === 0){
            getRestaurants(latLng, cuisines)
            .then(restResults =>{
                if (restResults.length === 0){
                    setRestToDisplay({name:"none"})
                }else{
                    document.querySelector("#cuisine-dropdown").classList.add("disabled")
                    setRestToDisplay(restResults[counter])
                    setRestaurants(restResults)
                    setCounter(counter+1)
                    document.getElementById("suggestion-button").innerText = "Try again"
                    document.getElementById("cuisine-header").innerText = "Here is your suggestion, but feel free to give it another go!"
                }
            })
            .catch(()=>{
                alert("Oops, something went wrong! :(")
            })
            
        }else{
            if (counter < restautants.length){
                setCounter(counter+1)
                setRestToDisplay(restautants[counter])
            }else{
                alert("Out of suggestions, please select another catergory")
                setRestToDisplay({})
                setRestaurants([])
                document.getElementById("suggestion-button").innerText = "Hit me"
                document.querySelector("#cuisine-dropdown").classList.remove("disabled")
            }
            
        }
    }

    const handleLocation = (e) =>{
        e.preventDefault()
        

        let tmpPromise
        if(e.target.id === "current-location" || e.target.parentElement.id === "current-location"){
            tmpPromise = Promise.resolve(currentLocation())
        }else{
            tmpPromise = Promise.resolve(zipToLatLong(zip))
        }
        tmpPromise
        .then(data =>{
            toggleZip(true)
            document.querySelector("#zip-submit").remove()
            document.querySelector("#current-location").remove()
            document.querySelector("#zip-or").remove()
            document.querySelector("#zip-header").innerText = "Desired location"
            document.querySelector("#search").classList.remove("action")
            setLatLng(data)
            setZip(data.zip)
            return getCuisines(data)
        })
        .then(setOptions)
        .catch(alert)
    }

    return (
        <div>
            <Image src='/pizzaLogo.png' size='tiny' centered />
            <h1>Whatcha gonna eat?</h1>
            <form action="#" id="location-form" onSubmit={handleLocation}>
                <h3 id="zip-header">Please enter your zip code below to get food recommendations</h3>
                <div id="search" className="ui action input">
                    <input placeholder="Enter your zip code" type="text" value={zip} onChange={(e)=>setZip(e.target.value)} disabled={zipInput}/>
                    <button type="submit" id="zip-submit"class="ui green button">Submit</button>
                </div>
                <div id="zip-or" style={{paddingTop:"10px"}}> or </div>
                <Button id="current-location" basic color="blue" className="link" onClick = {handleLocation}><h4> <Icon name="map marker alternate"/> Use current location</h4></Button>
            </form>
            <br/>
            { zipInput? <CuisineSelection options={options} handleChange={handleChange} handleSuggestion={handleSuggestion}/> : null}
            <DisplayRestaurant {...restToDisplay}/>
        </div>
    )
}
