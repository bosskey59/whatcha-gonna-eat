import React from 'react'
import {Loader, Grid, Header} from 'semantic-ui-react'

 

export default function DisplayRestaurant({cuisine, hours, location, name, phone, rating }) {

    const conditionalRender = () => {
        if (name === "loading"){
            return(
                <div>
                    <Loader active inline='centered' content='Loading' size='large'/>
                </div>
            )
        }else if(name === "none"){
            return (
                <div>
                    <h2>Sorry, no restaurants found!</h2>
                </div>  
            )
    
        }
        else if(!!name){
            return (
                
                <Grid >
                    {console.log(name+location.address)}
                    <Grid.Row style={{height: '45vh'}}>
                        <Grid.Column width={2}>
                        </Grid.Column>
                        <Grid.Column width={7}>
                            <iframe title="map" width="100%" height="100%" id="gmap_canvas" src={`https://maps.google.com/maps?q=${name+" "+location.address}&t=&z=12&ie=UTF8&iwloc=&output=embed`} frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                        </Grid.Column>
                        <Grid.Column  width={4} style={{textAlign:"left"}} >
                            <h2>{name}</h2>
                            <hr/>
                            <div>
                                <Header sub>Address</Header>
                                <span>{location.address}</span>
                            </div>
                            <hr/>
                            <div>
                                <Header sub>Phone Number</Header>
                                <span>{phone}</span>
                            </div>
                            <hr/>
                            <div>
                                <Header sub>Cuisines</Header>
                                <span>{cuisine}</span>
                            </div>
                            <hr/>
                            <div>
                                <Header sub>Rating</Header>
                                <span>{rating}</span>
                            </div>
                            <hr/>
                            { !hours ? null :
                            <div>
                                <Header sub>Hours of operation</Header>
                                <span>{hours}</span>
                            </div>
                            }
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            )
    
        }else{
            return null
        }
    }


    return (
        <div>
            <br/>
            {conditionalRender()}
        </div>
    )
}
