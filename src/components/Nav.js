import React, { useState } from 'react'
import { Menu } from 'semantic-ui-react'
import { useHistory } from "react-router-dom";


export default () => {
    const [active, setActive] = useState({})
    const history = useHistory()

    const handleItemClick = (e, { name }) => {
        setActive({ activeItem: name })
        history.push(`/${name}`)
    }

    const { activeItem } = active
    return (
        <Menu>
            <Menu.Item
                name='home'
                active={activeItem === 'home'}
                onClick={handleItemClick}
            >
                Home
            </Menu.Item>

            <Menu.Item
                name='about'
                active={activeItem === 'about'}
                onClick={handleItemClick}
            >
                About
            </Menu.Item>
        </Menu>
    )
}