import React from 'react'
import {Grid} from 'semantic-ui-react'

export default function About() {
    return (
        <div>
            
            <Grid centered columns={2}>
                <Grid.Row centered>
                    <Grid.Column style={{textAlign:"center"}}>
                    <h3>Hello! Whatcha gonna eat?</h3>
                    <p>
                        We've all been there before. You're really hungry but have no idea of where to go eat! 
                        This app is meant to help you decide by giving you options in your area of choice. 
                        Based on your location & cuisine choices, we will find restaurants near you that match 
                        what you're looking for.
                    </p>
                    <p>
                        Really have no clue at all? We will suggest a completely random restaurant for you. 
                        Click "Hit me" and have fun exploring your local food shops!
                    </p>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
            
        </div>
    )
}