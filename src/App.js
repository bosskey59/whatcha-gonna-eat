import React from 'react';
import './App.css';
import {
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import {Segment} from 'semantic-ui-react'
import Home from "./components/Home"
import About from "./components/About"
import Nav from "./components/Nav"

function App() {
  return (
    <div className="App">
      <Nav/>
      <Switch>
        <Route path="/about" component={About} />
        <Route path="/" component={Home} />
        <Redirect to="/" />
      </Switch>
      <Segment inverted vertical style={{  position: 'absolute', bottom: '0', height:'5%', width:'100%' }}>
        <div style={{textAlign:"center"}}> Made with &#9825; by AAJ &copy; 2020  | Powered by Zomato API</div>
      </Segment>
    </div>
  );
}

export default App;
