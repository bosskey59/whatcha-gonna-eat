const options = {
    method: 'GET',
    mode: 'cors',
    headers: {
        'user-key': process.env.REACT_APP_ZOMATO_KEY
      }
}

export const getCuisines = async({lat, lng})=>{
    const resp = await fetch(`https://developers.zomato.com/api/v2.1/cuisines?lat=${lat}&lon=${lng}`, options)
    const data = await resp.json()
    return await data.cuisines.map(c => ({key:c.cuisine.cuisine_id, text:c.cuisine.cuisine_name, value:c.cuisine.cuisine_id}))
}

function shuffleArray(array) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
    return array
}


export const getRestaurants = async({lat,lng},cuisineIds) =>{
    const stringCuisines = cuisineIds.join(",")
    const resp = await fetch(`https://developers.zomato.com/api/v2.1/search?&lat=${lat}&lon=${lng}&radius=9000.00&cuisines=${stringCuisines}`, options)
    const data = await resp.json()

    const getAllResterauntDataPossible = async (totalResults) =>{
        let urls = []
        for (let i = 0; i < 100; i+=20){
            if ( i < totalResults){
                urls.push(`https://developers.zomato.com/api/v2.1/search?start=${i}&count=20&lat=${lat}&lon=${lng}&radius=5000&cuisines=${stringCuisines}`)
            }else{
                break
            }
        }

        const data = await Promise.all(urls.map(url => {
            return fetch(url, options)
            .then(resp => resp.json())
            .then(data =>{
                return data.restaurants.map((rest)=>({name:rest.restaurant.name, phone:rest.restaurant.phone_numbers, image:rest.restaurant.featured_image, rating: rest.restaurant.user_rating.aggregate_rating, location:rest.restaurant.location, cuisine: rest.restaurant.cuisines, hours: rest.restaurant.timings }))
            })
        }))
        
        return shuffleArray(data.flat())
    }

    return await getAllResterauntDataPossible(data.results_found)

    
}