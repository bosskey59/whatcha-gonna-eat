export const currentLocation = async(info)=>{
    const resp = await fetch("https://ipapi.co/json/")
    const data = await resp.json()
    return {lat:data.latitude, lng:data.longitude, zip:data.postal}
}

export const zipToLatLong = async(zipCode) =>{
    if (/^[0-9]{5}(?:-[0-9]{4})?$/.test(zipCode)){
        const resp = await fetch(`https://www.mapquestapi.com/geocoding/v1/address?key=${process.env.REACT_APP_MAP_QUEST_KEY}&location=${zipCode}`)
        const data = await resp.json()
        return {...data.results[0].locations[0].latLng, zip:zipCode}
    }else{
        throw new Error('Please enter a valid zip code and try again!' )
    }
}